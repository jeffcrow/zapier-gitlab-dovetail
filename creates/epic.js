const sample = require('../samples/sample_epic');

const createEpic = (z, bundle) => {
  const responsePromise = z.request({
    method: 'POST',
    url: `https://gitlab.com/api/v4/groups/${bundle.inputData.group}/epics`,
    body: JSON.stringify({
      title: bundle.inputData.title,
      description: bundle.inputData.description
    })
  });
  return responsePromise
    .then(response => JSON.parse(response.content));
};

module.exports = {
  key: 'epic',
  noun: 'Epic',

  display: {
    label: 'Create Epic',
    description: 'Creates an epic.'
  },

  operation: {
    inputFields: [
      {key: 'group', label:'Group', required: true, dynamic: 'group.name'},
      {key: 'title', label:'Title', required: true},
      {key: 'description', label:'Description', required: false}
    ],
    perform: createEpic,
    sample: sample
  }
};
