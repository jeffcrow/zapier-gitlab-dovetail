const sample = require('../samples/sample_epic');

const triggerEpic = (z, bundle) => {
  const responsePromise = z.request({
    method: 'GET',
    url: `https://gitlab.com/api/v4/groups/${bundle.inputData.group}/epics`,
    params: {
      state: bundle.inputData.state,
    }
  });
  return responsePromise
    .then(response => JSON.parse(response.content));
};

module.exports = {
  key: 'epic',
  noun: 'Epic',

  display: {
    label: 'Get Epics',
    description: 'Triggers on a new epic.'
  },

  operation: {
    inputFields: [
      {key: 'group', label:'Group', required: true, dynamic: 'group.name'},
      {key:'state', required: false, label: 'State', choices: {open:'open',closed:'closed',all:'all'}, helpText:'Default is "open"'}
    ],
    perform: triggerEpic,

    sample: sample
  }
};
