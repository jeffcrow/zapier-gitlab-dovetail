const sample = require('../samples/sample_group_list');

const triggerGroup = (z, bundle) => {
  const responsePromise = z.request({
    url: 'https://gitlab.com/api/v4/groups',
    params: {
      owned: bundle.inputData.Owned,
    }
  });
  return responsePromise
    .then(response => JSON.parse(response.content));
};

module.exports = {
  key: 'group',
  noun: 'Group',

  display: {
    label: 'Get Group',
    hidden: true,
    description: 'The only purpose of this trigger is to populate the dropdown list of groups in the UI, thus, it\'s hidden.'
  },

  operation: {
    inputFields: [
      {key:'owned', required: false, label: 'Owned', choices: {true:'True',false:'false'}, helpText:'Limit to groups owned by the user.'}
    ],
    perform: triggerGroup,
    sample: sample
  }
};
